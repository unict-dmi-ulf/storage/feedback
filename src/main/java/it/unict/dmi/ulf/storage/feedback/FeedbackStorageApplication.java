package it.unict.dmi.ulf.storage.feedback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedbackStorageApplication {
	public static void main(String[] args) {
		SpringApplication.run(FeedbackStorageApplication.class, args);
	}

}
