package it.unict.dmi.ulf.storage.feedback.entity;

import java.net.URI;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "token", "lecture" }) })
public class Feedback {
    @Id
    @GeneratedValue
    long id;

    @NotNull
    URI token;

    @NotNull
    URI lecture;

    @ElementCollection
    @CollectionTable(name = "feedback_answers", joinColumns = @JoinColumn(name = "feedbackId"))
    List<String> answers;
}
