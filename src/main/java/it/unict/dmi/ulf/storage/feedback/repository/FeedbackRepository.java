package it.unict.dmi.ulf.storage.feedback.repository;

import org.springframework.data.repository.CrudRepository;

import it.unict.dmi.ulf.storage.feedback.entity.Feedback;

public interface FeedbackRepository extends CrudRepository<Feedback, Long> {
}
